// Saves options.
function save_options() {
    chrome.storage.sync.get('jsApiPath', function(result) {
        if ($('#jsApiPath').val() != ''){
            chrome.storage.sync.set({'jsApiPath': $('#jsApiPath').val()}, function() {
                chrome.storage.sync.set({'back_reload': 1}, function() {
                  window.location.reload();
                });
            });
        }
    });

    chrome.storage.sync.get('baseApp', function(result) {
        if ($('#baseApp').val() != ''){
            chrome.storage.sync.set({'baseApp': $('#baseApp').val()}, function() {
                console.log('Settings saved');
            });
        }
    });
    var status = document.getElementById("status");
    status.innerHTML = "Saved.";
    setTimeout(function() {
        status.innerHTML = "";
    }, 750);
}

// Restore
function restore_options() {
    chrome.storage.sync.get('jsApiPath', function(result) {
        $('#jsApiPath').val(result["jsApiPath"])
    });

    chrome.storage.sync.get('baseApp', function(result) {
        $('#baseApp').val(result["baseApp"])
    });    
}

$(document).ready(function() {
    restore_options();
    $('#save').click(save_options);
});

