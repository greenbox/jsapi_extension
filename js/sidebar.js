/** Check if needed **/
function toggleSideBar(url) {
    chrome.storage.sync.get('jsApiPath', function(result) {
        if (typeof result['jsApiPath'] == 'undefined'){
            if(confirm("JSAPI not configured: Do it now ?")) {
                chrome.tabs.create( { url: chrome.extension.getURL('options.html') });
            }
            return;
        }else{
            chrome.tabs.create( { url: 'https://bitbucket.org/greenbox' });
        }

        var scripts = [ "js/jquery-1.9.0.js", "js/jquery-ui-1.10.0.custom.js", "js/toggle.js" ];
    });
}

/** check if needed */
// React when a browser action's icon is clicked.
chrome.browserAction.onClicked.addListener(function(tab) {
    toggleSideBar();
});


/** JsAPI part */
console.log('JSAPI init');
var jsApiPath = '';
var baseApp = '';

// base app holder [for app generator]
BASE = {};

function jsx(meth){ 
  baseData = {};

  if (typeof meth == 'undefined')
    return false;

  if (typeof para == 'object'){
    $.extend(baseData, para);
  }
  $.ajax({
    type: "GET",
    url: jsApiPath + 'lt.php',
    data: { uri : '/blank_app/link/' + meth + '.app.js'} ,
    success: eval,
    error: function(result){ 
      eval(result.responseText); 
    }
  });
}

function jsxc(meth , serve){
  fnctr = false;
  if (typeof serve != 'undefined' && serve == 'serve'){
    if (typeof window.serveCont == 'undefined')
        console.log('please define "window.serveCont" for jsxc usage');
    else{
        fnctr = function (content) {
            serveCont(content , meth);
        };
    }
  }else{
    if (typeof window.prepCont == 'undefined')
        console.log('please define "window.prepCont" for jsxc usage');
    else{
        fnctr = prepCont;
    }
  }

  if (typeof meth == 'undefined')
    return false;

  if (typeof para == 'object'){
    $.extend(baseData, para);
  }
  $.ajax({
    type: "GET",
    url: jsApiPath + 'lt.php',
    data: { uri : '/blank_app/link/' + meth + '.app.js'} ,
    success: fnctr,
    error: function(result){ 
      fnctr(result.responseText); 
    }
  });
}

chrome.storage.sync.get('jsApiPath', function(result) {
    if (typeof result['jsApiPath'] != 'undefined' && result['jsApiPath'] != '')        
        defineApiPath(result['jsApiPath']);
    else{
        //chrome.tabs.create( { url: chrome.extension.getURL('options.html') });
        defineApiPath('http://demo.2p.lv/');
    }
        
});

setInterval(function(){
    chrome.storage.sync.get('back_reload', function(result) {
      if (result.back_reload == 1){
         chrome.contextMenus.removeAll(function() {
          chrome.storage.sync.set({'back_reload': 0}, function() {
            window.location.reload();
          });
        });
      }
    });
},3000);

chrome.contextMenus.create({
    "title": "Reload JSAPI",
    "contexts": ["page", "selection", "image", "link"],
    "onclick" : function (e){
      chrome.contextMenus.removeAll(function() {
          window.location.reload();
      });
    }
});

chrome.contextMenus.create({
    "title": "JSAPI Options",
    "contexts": ["page", "selection", "image", "link"],
    "onclick" : function (e){
      chrome.tabs.create( { url: chrome.extension.getURL('options.html') });
    }
});

chrome.contextMenus.create({
    "title": "JSAPI BackGround",
    "contexts": ["page", "selection", "image", "link"],
    "onclick" : function (e){
      chrome.tabs.create( { url: chrome.extension.getURL('_generated_background_page.html') });
    }
});




function defineApiPath(ApiPath){
    jsApiPath = ApiPath;

    //get jQuery here
    var jQ = document.createElement('script');
    jQ.type = "text/javascript";
    jQ.src = 'js/jquery-1.9.0.js';
    document.body.appendChild(jQ);

    setTimeout(function(){
        $( document ).ready(function() {
            chrome.storage.sync.get('baseApp', function(result) {
                if (typeof result['baseApp'] != 'undefined' && result['baseApp'] != '')        
                    jsx(result['baseApp']);
                else
                    jsx('crx2p');
                    
                
            });  
        });
    },125);
}

/* End JsAPI part */
